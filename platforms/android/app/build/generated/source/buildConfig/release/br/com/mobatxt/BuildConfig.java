/**
 * Automatically generated file. DO NOT MODIFY
 */
package br.com.mobatxt;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "br.com.mobatxt";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.0.0";
  // Fields from default config.
  public static final long _BUILDINFO_TIMESTAMP = 1542403746035L;
}
